package com.example.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController

@RequestMapping("/api/catalog")
public class RoomateCatalogApi {
    @Autowired
    private RestTemplate restTemplate;



    @GetMapping("/{userId}")

    public List<RoommateCatalog> getAllRoommates(

            @PathVariable String userId) {


        UserRoommate  userRoommate = restTemplate.getForObject(

                "http://http://localhost:8082/roommate/info/" + userId,

                UserRoommate.class);


        List<RoommateCatalog> roommateCatalogList = new ArrayList<>();



        for (Roommate roommate : userRoommate.getUserRoommates()) {

            Rating roommateRating = restTemplate.getForObject(

                    "http://localhost:8083/rating/" + roommate.getId(),

                    Rating.class);



           roommateCatalogList.add(new RoommateCatalog(roommate.getName(),

                    roommate.getGender(), roommateRating.getRating()));

        }



        return roommateCatalogList;

    }
}
